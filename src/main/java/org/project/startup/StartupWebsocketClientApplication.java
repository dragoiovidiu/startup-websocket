package org.project.startup;

import org.project.startup.websocket.service.Processor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableBinding(Processor.class)
public class StartupWebsocketClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(StartupWebsocketClientApplication.class, args);
	}
}
