package org.project.startup.websocket.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.socket.WebSocketHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import reactor.core.publisher.Mono;
import org.springframework.web.reactive.socket.WebSocketSession;

abstract class UserParsingHandshakeHandler implements WebSocketHandler {

    private final static Logger LOG = LoggerFactory.getLogger(UserParsingHandshakeHandler.class);
    private final Map<String, String> sessionUuidMap;
    private final Map<String, String> uuidUserMap;

    UserParsingHandshakeHandler() {
        this.sessionUuidMap = new HashMap<>();
        this.uuidUserMap = new HashMap<>();
    }

    @Override
    public final Mono<Void> handle(WebSocketSession session) {
    	
    	//put unique identifier for this user, the userMap contains an unique identifier
        //key = websocket session, value = uuid generated on the client
        this.sessionUuidMap.put(session.getId(),
                Stream.of(session.getHandshakeInfo().getUri()
                        .getQuery().split("&"))
                        .map(s -> s.split("="))
                        .filter(strings -> strings[0].equals("uuid"))
                        .findFirst()
                        .map(strings -> strings[1])
                        .orElse(""));
        
        //key = uuid generated on client , value = user send on query connection
        this.uuidUserMap.put(
        		
        		Stream.of(session.getHandshakeInfo().getUri()
		                .getQuery().split("&"))
		                .map(s -> s.split("="))
		                .filter(strings -> strings[0].equals("uuid"))
		                .findFirst()
		                .map(strings -> strings[1])
		                .orElse(""),
		                
                Stream.of(session.getHandshakeInfo().getUri()
                        .getQuery().split("&"))
                        .map(s -> s.split("="))
                        .filter(strings -> strings[0].equals("user"))
                        .findFirst()
                        .map(strings -> strings[1])
                        .orElse("")
                        
                        );

        LOG.info("User websocket sessionId: {}", session.getId());
        LOG.info("User linked to the session: {}", getUuidUserMap(getSessionUuidMap(session.getId())));
        LOG.info("SESSIONS : {}" + sessionUuidMap);
        return handleInternal(session);
    }

    abstract protected Mono<Void> handleInternal(WebSocketSession session);

    /**
     * get UUID based on session id
     * @param id
     * @return
     */
    String getSessionUuidMap(String id) {
        return sessionUuidMap.get(id);
    }

    /**
     * get User based on UUID
     * @param user
     * @return
     */
    String getUuidUserMap(String user) {
        return uuidUserMap.get(user);
    }

    /**
     * remove session connection based on session->uuid
     * checks if uuid match
     *
     * @param uuid
     * @param session
     */
    void removesSessionUuidMap(String uuid, WebSocketSession session) {
    	if (sessionUuidMap == null) {
    		return;
    	}
    	for (String key : sessionUuidMap.keySet()) {
    		if (uuid == sessionUuidMap.get(key)) {
    			   LOG.info("Removing session at user disconnect: {}", key);
                    sessionUuidMap.remove(key);
	    	    	session.close();
    		}    	 
    	}
    }
}
