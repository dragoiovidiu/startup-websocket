package org.project.startup.websocket.service.impl;


import reactor.core.publisher.Mono;

import static org.project.startup.websocket.event.UserEvent.Type.USER_LEFT;

import org.project.startup.websocket.event.UserEvent;
import org.project.startup.websocket.model.UUIDSession;
import org.project.startup.websocket.model.User;
import org.project.startup.websocket.service.Processor;
import org.project.startup.websocket.util.UserUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;


@Service
public class InboundChatService extends UserParsingHandshakeHandler {
	
	@Autowired
	private  Processor chatServiceStreams;	
	private static final Logger LOG = LoggerFactory.getLogger(InboundChatService.class);
	
	public InboundChatService(Processor chatServiceStreams){
		this.chatServiceStreams = chatServiceStreams;
	}
	
	/**
	 * Handle incoming messages
	 */
	@Override
	protected Mono<Void> handleInternal(WebSocketSession session) {
		
		return session
			.receive()
			.log( getUuidUserMap(getSessionUuidMap(session.getId())) + "-i-incoming-chat-message")
			.map(WebSocketMessage::getPayloadAsText)
			.log(getUuidUserMap(getSessionUuidMap(session.getId()))+ "-i-convert-to-text")
			.flatMap(message ->  {				
				return broadcast(message, getSessionUuidMap(session.getId()));
			})
	
			.log(getUuidUserMap(getSessionUuidMap(session.getId())) + "-i-broadcast-to-broker")
			.doOnComplete(() -> {
			//send notification trough the broker that the user disconnected
				
				String user =  getUuidUserMap(getSessionUuidMap(session.getId()));
				String uuid = getSessionUuidMap(session.getId());
				
				UserEvent userEvent = UserEvent.type(USER_LEFT)
                .withPayload()
                .user(new User(uuid, "dsf"))
                .build();
				chatServiceStreams.clientToBroker().send(
						MessageBuilder
							.withPayload(UserUtil.toJSON(userEvent))
							.setHeader(Processor.USER_HEADER, user)
							.setHeader(Processor.UUID_HEADER, uuid)
							.build());
				System.out.println("DISCONNECTED");
			} )
			.then();
	}
  

	/**
	 * send incoming websocket message payload to the broker
	 * @param message
	 * @param uuid
	 * @return
	 */
	public Mono<?> broadcast(String message, String uuid) {
			return Mono.fromRunnable(() -> {			
				chatServiceStreams.clientToBroker().send(
					MessageBuilder
						.withPayload(message)
						//set the header on the broker side that contains the user name to be verified later
						.setHeader(Processor.UUID_HEADER, uuid)
						.setHeader(Processor.USER_HEADER, getUuidUserMap(uuid))
						.build());
			
		});
	} 
}
