package org.project.startup.websocket.service.impl;


import org.project.startup.websocket.event.MessageEvent;

import org.project.startup.websocket.event.UserEvent;
import org.project.startup.websocket.service.Processor;
import org.project.startup.websocket.subscriber.WebSocketMessageSubscriber;
import org.project.startup.websocket.util.UserUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.publisher.UnicastProcessor;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.socket.WebSocketSession;
import static org.project.startup.websocket.event.UserEvent.Type.PRIVATE_CHAT_MESSAGE;
import static org.project.startup.websocket.event.UserEvent.Type.USER_LEFT;

@Service
@EnableBinding(Processor.class)
public class OutboundChatService extends UserParsingHandshakeHandler {
	
	private final static Logger LOG =LoggerFactory.getLogger(OutboundChatService.class);	
	private Flux<Message<String>> flux;
	private FluxSink<Message<String>> chatMessageSink;	
	private UnicastProcessor<UserEvent> eventPublisher;	
 	private Flux<String> outputEvents;	
 	
	public OutboundChatService(UnicastProcessor<UserEvent> eventPublisher, Flux<UserEvent> events) {				
		//create flux to listen to broker
		this.flux = Flux.<Message<String>>create(
				emitter -> this.chatMessageSink = emitter,
				FluxSink.OverflowStrategy.IGNORE)
				.publish()
				.autoConnect();
		this.eventPublisher = eventPublisher;
		this.outputEvents = Flux.from(events).map(UserUtil::toJSON);		
	}
	
	/**
	 * indicates that this service will be listening forincoming messages on 
	 * the brokerToClient channel. When it receives one, it will forward it to
	 * chatMessageSink.
	 * @StreamListener(ChatServiceStreams.BROKER_TO_CLIENT) 
	 * @param message
	 */
	@StreamListener(Processor.BROKER_TO_CLIENT)
	public void listen(Message<String> message) {
		if (chatMessageSink != null) {
			chatMessageSink.next(message);
		}
	}
	
	/**
	 * handle the sending of messages to websocket client
	 */
	@Override
	protected Mono<Void> handleInternal(WebSocketSession session) {		
		  WebSocketMessageSubscriber subscriber = new WebSocketMessageSubscriber(eventPublisher);

		  //user
		  String user = getUuidUserMap(getSessionUuidMap(session.getId()));
		  
		  this.flux
          .log(user + "-o-from-broker-to-message-event")
		  .map(UserUtil::toMessageEvent)
          .log(user + "-o-message-event-filter-connection")
		  .filter(s -> validateUserFromInboundSocketConnection(s, getSessionUuidMap(session.getId())))
          .log(user + "-o-from-message-event-to-user-event")
		  .map(this::transformToUserEvent)
          .log(user + "-o-outputEvents-user-disconnect")
		  .map(userEvent -> validateUserDisconnected(userEvent, session))
          .log(user + "-o-subscribe-user-event-to-flux-incoming-message-from-broken")
          .subscribe(subscriber::onNext, subscriber::onError, subscriber::onComplete);	
		 
		 return session.send(outputEvents
                  .log(user + "-o-outputEvents-from-json-to-user-event")
				  .map(UserUtil::toEvent)
                  .log(user + "-o-outputEvents-filter-private-message")
				  .filter(userEvent -> validatePrivateMessage(userEvent, getSessionUuidMap(session.getId())))
                  .log(user + "-o-outputEvents-from-user-event-to-json")
				  .map(UserUtil::toJSON)
				  .map(session::textMessage))
			 	 .log(user +"-o-wrap-as-websocket-message")
				 .log(user +"-o-publish-to-websocket");

	}	

	/**
	 * Filter the inbound user to be the same with the one from outbound
	 * If not filtered correctly messages are sent on duplicates depending on how many connection are and how many UserEvents are created
	 * Practically are 3 user connected the are created 3 UserEvents , each UserEvent is corresponding to an user,
	 * A flux of UserEvents is created (3) this is iterated by websocket send() consumer 
	 * if the user is not checked to see if the UserEvent is corresponding to him all UserEvents are sent to him duplicated depending on the nr of events
	 * CHECKED: from inbound is coming an broker header [User = ?user=username ] attached To MessageEvent if the MessageEvent header is the same with the 
	 * user form outbound ?user=username  -> getUser(session.getId()) then the message is passed along 
	 * 
	 * 
	 * @param message
	 * @param uuidFromOutbound
	 * @return
	 */
	private boolean validateUserFromInboundSocketConnection(MessageEvent message, String uuidFromOutbound) {

	    //header from message queue
		String uuidFromInbound = message.getPayload().getMessage().getHeader().get(Processor.UUID_HEADER, String.class);
		String userFromInbound = message.getPayload().getMessage().getHeader().get(Processor.USER_HEADER, String.class);

		String userFromOutbound = getUuidUserMap(uuidFromOutbound);
		
		//verify if is the same user and is the same uuid (both from client listen to messages)
		return uuidFromOutbound.equals(uuidFromInbound) && userFromOutbound.equals(userFromInbound);
		
	}
	
    /**
     *
     * @param message
     * @param uuid
     * @return
     */
	private boolean validatePrivateMessage(UserEvent message, String uuid) {
		if (message.getType() == PRIVATE_CHAT_MESSAGE) {
			String targetUser =  (String) message.getProperties().get("targetUser");

            System.out.println("SENDDER: D" + message.getProperties().get("sender"));
            //if user connected is the same from incoming message targetUser or
            return getUuidUserMap(uuid).equals(targetUser) || uuid.equals(message.getProperties().get("sender"));
		} 
		 else {
			return true;
		}
	}

	/**
	 *
	 * @param userEvent
	 * @param session
	 * @return
	 */
	private UserEvent validateUserDisconnected (UserEvent userEvent,  WebSocketSession session) {
		if (userEvent.getType() == USER_LEFT) {
			removesSessionUuidMap(getUuidUserMap(session.getId()), session);
		}
		return userEvent;
	}
	
	/**
	 * Transform from MessageEvent to UserEvent
	 * Get the sender from MessageEvent and put in UserEvent for further filtering 
	 * 
	 * @param messageEvent
	 * @return
	 */
	private UserEvent transformToUserEvent(MessageEvent messageEvent) {
		String sender = messageEvent.getPayload().getMessage().getHeader().get(Processor.UUID_HEADER, String.class);
		UserEvent  userEvent=  UserUtil.toEvent(messageEvent.getPayload().getMessage().getMessagePayload());		
		userEvent.setProperties("sender", sender);		
		return userEvent;		 
		 
	}
}
