package org.project.startup.websocket.model;

import java.util.ArrayList;
import java.util.List;

public class UUIDSession {
	private String user;
	private List<String> UUID;
	
	public UUIDSession() {
		
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	public String getUser() {
		return user;
	}
	
	public List<String> getUUID() {
		return UUID;
	}
	public void addUUID(String uuidString) {
		if (UUID == null) {
			UUID = new ArrayList<>();
		}
		UUID.add(uuidString);
	}	
	
}
