package org.project.startup.websocket.model;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import com.fasterxml.jackson.annotation.JsonCreator;

public class MessageEntity {
	
	private String payload;
	private MessageHeaders header;

	@JsonCreator
    public MessageEntity(String payload, MessageHeaders header) {
        this.payload = payload;
        this.header = header;
    }

    public String getMessagePayload() {
        return payload;
    }

    public MessageHeaders getHeader() {
        return header;
    }
}
