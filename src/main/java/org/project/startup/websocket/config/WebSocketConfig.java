package org.project.startup.websocket.config;

import java.util.HashMap;
import java.util.Map;

import org.project.startup.websocket.event.UserEvent;
import org.project.startup.websocket.service.Processor;
import org.project.startup.websocket.service.impl.InboundChatService;
import org.project.startup.websocket.service.impl.OutboundChatService;
import org.project.startup.websocket.subscriber.UserStats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;

import reactor.core.publisher.Flux;
import reactor.core.publisher.UnicastProcessor;

@Configuration
public class WebSocketConfig {
	
	@Autowired
	private  Processor chatServiceStreams;
	
	@Bean
	public UnicastProcessor<UserEvent> eventPublisher() {
		return UnicastProcessor.create();
	}

	@Bean
	public Flux<UserEvent> events(UnicastProcessor<UserEvent> eventPublisher) {
		//replay last 25 messages
		return eventPublisher.replay(0).autoConnect();
	}


	@Bean
	HandlerMapping webSocketMapping(InboundChatService inboundChatService, OutboundChatService outboundChatService, UnicastProcessor<UserEvent> eventPublisher, Flux<UserEvent> events) {
		Map<String, WebSocketHandler> urlMap = new HashMap<>();
		urlMap.put("/app/chatMessage.new", inboundChatService);		
		urlMap.put("/topic/chatMessage.new", outboundChatService);
		Map<String, CorsConfiguration> corsConfigurationMap = new HashMap<>();
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.addAllowedOrigin("*");
		corsConfigurationMap.put("/app/chatMessage.new", corsConfiguration);
		corsConfigurationMap.put("/topic/chatMessage.new", corsConfiguration);

		SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
		mapping.setOrder(10);
		mapping.setUrlMap(urlMap);
		mapping.setCorsConfigurations(corsConfigurationMap);

		return mapping;
	}
	
	@Bean
	public UserStats userStats(Flux<UserEvent> events, UnicastProcessor eventPublisher) {
		return new UserStats(events, eventPublisher);
	}

	@Bean
	WebSocketHandlerAdapter handlerAdapter() {
		return new WebSocketHandlerAdapter();
	}
}
