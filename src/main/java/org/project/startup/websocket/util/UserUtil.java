package org.project.startup.websocket.util;

import java.io.IOException;

import org.project.startup.websocket.event.MessageEvent;
import org.project.startup.websocket.event.MessageEventBuilder;
import org.project.startup.websocket.event.UserEvent;
import org.project.startup.websocket.model.MessageEntity;
import org.project.startup.websocket.model.User;
import org.springframework.messaging.Message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.project.startup.websocket.event.MessageEvent.Type.PRIVATE_CHAT_MESSAGE;

public class UserUtil {

	public static UserEvent toEvent(String json) {
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, UserEvent.class);
		} catch (IOException e) {
			throw new RuntimeException("Invalid JSON:" + json, e);
		}
	}

	public static String toJSON(UserEvent event) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(event);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static MessageEvent toMessageEvent(Message<String> message) {		
		
			MessageEvent messageEvent = MessageEvent.type(PRIVATE_CHAT_MESSAGE)
	                .withPayload()
	                .message(new MessageEntity(message.getPayload(), message.getHeaders()))
	                .build();
			
			return messageEvent;
	
	}
}
