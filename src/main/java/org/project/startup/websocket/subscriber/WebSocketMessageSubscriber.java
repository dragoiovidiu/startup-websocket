package org.project.startup.websocket.subscriber;

import static org.project.startup.websocket.event.UserEvent.Type.USER_LEFT;

import java.util.Optional;

import org.project.startup.websocket.event.UserEvent;

import reactor.core.publisher.UnicastProcessor;

 public class WebSocketMessageSubscriber {
	 
	 private UnicastProcessor<UserEvent> eventPublisher;
     private Optional<UserEvent> lastReceivedEvent = Optional.empty();

     public WebSocketMessageSubscriber(UnicastProcessor<UserEvent> eventPublisher) {
         this.eventPublisher = eventPublisher;
     }

     public void onNext(UserEvent event) {
         lastReceivedEvent = Optional.of(event);
         eventPublisher.onNext(event);
     }

     public void onError(Throwable error) {
         //TODO log error
         error.printStackTrace();
     }

     public void onComplete() {
         System.out.println("ON COMPLETE");
         lastReceivedEvent.ifPresent(event -> eventPublisher.onNext(
         		UserEvent.type(USER_LEFT)
                         .withPayload()
                         .user(event.getUser())
                         .build()));
     }
}
